# ElizavetaGrinberg_levelup_advanced

### Запуск тестов в IDEA
Для запуска APi и UI тестов используем команду:
 `mvn clean test`

В maven существует возможность изменять настройки проекта в зависимости от окружения, это реализовано через профили проекта. 
Профиль содержит в себе настройки, которые будут добавлены к основным настройкам, или, наоборот, заместят их.

Профиль можно активировать явно, указав в строке запуска мавена имя профиля через опцию -P:
 `mvn -Pимя_профиля`

Для запуска API тестов используем команду:
 `mvn clean test -P API`


### Запуск тестов в Docker-контейнере
Сначала стартуем Docker desktop, что бы запустился Docker daemon.

Собираем Docker-образ проекта, выполнив (из корня 'cd Absolute Path') команду в консоли:
`docker build . -t levelp_adv_hm2:0.0.1`

Для сборки проекта и запуска тестов используем команду:
 `docker run levelp_adv_hm2:0.0.1`

Для выборочного запуска API или UI тестов указывается соответствующее значение в параметре `testTypes`:
 `docker run -e testTypes=UI levelp_adv_hm2:0.0.1`


----------------------------------------

## Getting started

To make it easy for you to get started with GitLab, here's a list of recommended next steps.

Already a pro? Just edit this README.md and make it your own. Want to make it easy? [Use the template at the bottom](#editing-this-readme)!

## Add your files

- [ ] [Create](https://docs.gitlab.com/ee/user/project/repository/web_editor.html#create-a-file) or [upload](https://docs.gitlab.com/ee/user/project/repository/web_editor.html#upload-a-file) files
- [ ] [Add files using the command line](https://docs.gitlab.com/ee/gitlab-basics/add-file.html#add-a-file-using-the-command-line) or push an existing Git repository with the following command:

```
cd existing_repo
git remote add origin https://gitlab.com/elvoroni/elizavetagrinberg_levelup_advanced.git
git branch -M main
git push -uf origin main
```

## Integrate with your tools

- [ ] [Set up project integrations](https://gitlab.com/elvoroni/elizavetagrinberg_levelup_advanced/-/settings/integrations)

## Collaborate with your team

- [ ] [Invite team members and collaborators](https://docs.gitlab.com/ee/user/project/members/)
- [ ] [Create a new merge request](https://docs.gitlab.com/ee/user/project/merge_requests/creating_merge_requests.html)
- [ ] [Automatically close issues from merge requests](https://docs.gitlab.com/ee/user/project/issues/managing_issues.html#closing-issues-automatically)
- [ ] [Enable merge request approvals](https://docs.gitlab.com/ee/user/project/merge_requests/approvals/)
- [ ] [Set auto-merge](https://docs.gitlab.com/ee/user/project/merge_requests/merge_when_pipeline_succeeds.html)

## Test and Deploy

Use the built-in continuous integration in GitLab.

- [ ] [Get started with GitLab CI/CD](https://docs.gitlab.com/ee/ci/quick_start/index.html)
- [ ] [Analyze your code for known vulnerabilities with Static Application Security Testing (SAST)](https://docs.gitlab.com/ee/user/application_security/sast/)
- [ ] [Deploy to Kubernetes, Amazon EC2, or Amazon ECS using Auto Deploy](https://docs.gitlab.com/ee/topics/autodevops/requirements.html)
- [ ] [Use pull-based deployments for improved Kubernetes management](https://docs.gitlab.com/ee/user/clusters/agent/)
- [ ] [Set up protected environments](https://docs.gitlab.com/ee/ci/environments/protected_environments.html)
