FROM markhobson/maven-chrome

WORKDIR /app

COPY ./src src/
COPY ./pom.xml pom.xml
COPY ./properties.json properties.json
COPY ./properties_api.json properties_api.json

ENV API_CONFIG_PATH=/app/properties_api.json
ENV WEB_UI_CONFIG_PATH=/app/properties.json
ENV testTypes=ALL

RUN mvn clean package -DskipTests

#ENTRYPOINT ["mvn", "clean", "test"]

CMD mvn test -P $testTypes