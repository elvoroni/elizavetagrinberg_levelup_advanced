package ru.levelup.at.advance.homework1.configweb;

public record WebUiConfiguration(
        String url,
        Boolean headless
) {

}
