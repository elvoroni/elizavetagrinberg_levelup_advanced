package ru.levelup.at.advance.homework1.web;

import com.codeborne.selenide.ElementsCollection;
import com.codeborne.selenide.Selectors;
import com.codeborne.selenide.Selenide;
import com.codeborne.selenide.SelenideElement;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

import static com.codeborne.selenide.Selectors.byAttribute;
import static com.codeborne.selenide.Selectors.byId;
import static com.codeborne.selenide.Selenide.$;

public class TransactionPage {

    public SelenideElement resetButton = $(byAttribute("ng-click^", "reset"));
    public SelenideElement backButton = $(byAttribute("ng-click^", "back"));
    private final SelenideElement row = $(byId("anchor0"));

    public Boolean tableRowEmpty(){
        return row.isDisplayed();
    }

    public String getDateTimeOfTransaction() {
        return row.$$("td").get(0).getText().trim();
    }

    public String getAmountOfMoney() {
        return row.$$("td").get(1).getText().trim();
    }


    public String getCurrentTime() {
        SimpleDateFormat formatter = new SimpleDateFormat("MMM d, yyyy h:mm:ss a", Locale.ENGLISH);
        Date date = new Date();
        return formatter.format(date);
    }
}
