package ru.levelup.at.advance.homework1.web;

import static com.codeborne.selenide.Condition.visible;
import static ru.levelup.at.advance.homework1.configweb.Currency.RUPEE;

import com.codeborne.selenide.Condition;
import com.codeborne.selenide.Selenide;
import io.qameta.allure.Step;
import junit.framework.Assert;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;
import ru.levelup.at.advance.homework1.configweb.Currency;

@DisplayName("Web тесты")
public class CreateDepositTest extends AbstractBaseTest {
    CustomerPage customerPage = new CustomerPage();
    TransactionPage transactionsPage = new TransactionPage();

    @Test
    @Tag("ui")
    @DisplayName("Тест на создание депозита")
    void testCreateDeposit() {
        var loginPage = new LoginPage();
        loginPage.login(LoginPage.Login.Customer);
        selectCustomer();
        selectCurrency(RUPEE);
        resetTransactionsHistory();
        String amount = setDeposit();
        String submitTime = getCurrentTime();
        transaction(amount, submitTime);
    }

    @Step("Выбор клиента")
    void selectCustomer() {
        var customer = new SelectCustomerPage();
        customer.selectRandomCustomerFromList();
        customer.loginButton().shouldBe(visible).click();
    }

    @Step("Выбор валюты")
    void selectCurrency(Currency currency) {
        customerPage.selectAccountNumber(currency);
    }

    @Step("Удаление предыдущей истории транзакций")
    private void resetTransactionsHistory() {
        customerPage.transactionButton().click();
        if (transactionsPage.tableRowEmpty()) {
            transactionsPage.resetButton.shouldBe(Condition.visible).click();
        }
        transactionsPage.backButton.shouldBe(Condition.visible).click();
    }


    @Step("Внести депозит на счёт в случайном размере")
    String setDeposit() {
        customerPage.depositButton().shouldBe(visible).click();
        String amount = customerPage.amountGeneration();
        customerPage.amountSet().setValue(amount);
        customerPage.buttonSubmitProcess().click();
        Assert.assertEquals("Проверка суммы депозита", customerPage.balance().getText(), amount);
        return amount;
    }

    @Step("Сохранение даты и времени пополнения")
    String getCurrentTime() {
        String submitTime = transactionsPage.getCurrentTime();
        customerPage.transactionButton().shouldBe(visible).click();
        return submitTime;
    }

    @Step("Проверка транзакции в списке транзакций")
    void transaction(String expectedAmount, String expectedTime) {
        do {
            Selenide.refresh();
        }
        while (!transactionsPage.tableRowEmpty());
        String actualTime = transactionsPage.getDateTimeOfTransaction();
        Assert.assertEquals("Проверка даты и времени пополнения", expectedTime, actualTime);
        String actualAmount = transactionsPage.getAmountOfMoney();
        Assert.assertEquals("Проверка суммы пополнения", expectedAmount, actualAmount);
    }
}
