package ru.levelup.at.advance.homework1.web;

import com.codeborne.selenide.Configuration;
import com.codeborne.selenide.Selenide;
import com.codeborne.selenide.logevents.SelenideLogger;
import io.qameta.allure.selenide.AllureSelenide;
import jdk.jfr.Category;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.remote.DesiredCapabilities;
import ru.levelup.at.advance.homework1.configweb.WebUiConfiguration;
import ru.levelup.at.advance.homework1.configweb.WebUiConfigurationProvider;

public class AbstractBaseTest {

    protected WebUiConfiguration config;

    @BeforeEach
    void setUp() {
        SelenideLogger.addListener("Selenide Listener", new AllureSelenide());
        config = WebUiConfigurationProvider.getConfig();

        ChromeOptions options = new ChromeOptions();
        options.addArguments("--disable-extensions");
        options.addArguments("--no-sandbox");

        Configuration.browserCapabilities = new DesiredCapabilities();
        Configuration.browserCapabilities.setCapability(ChromeOptions.CAPABILITY, options);
        Configuration.timeout = 10000;
        Configuration.headless = config.headless();

        Selenide.open(config.url());
    }

    @AfterEach
    void tearDown() {
        Selenide.closeWebDriver();
    }
}
